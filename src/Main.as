package {
    import flash.display.Bitmap;
    import flash.display.Shape;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.text.TextField;
    import vectorizer.Vectorizer;

    public class Main extends Sprite {

        [Embed(source = "../bin/test.png")]
        public var bmpClass:Class;

        public var shape:Shape;

        public function Main():void {
            if (stage)
                init();
            else
                addEventListener(Event.ADDED_TO_STAGE, init);
        }

        private function init(e:Event = null):void {
            removeEventListener(Event.ADDED_TO_STAGE, init);

            var bitmap:Bitmap = new bmpClass();

            var vectorize:Vectorizer = new Vectorizer(bitmap.bitmapData);
            var path:Vector.<Point> = vectorize.getShape(0xFFFF0000, '==', 0.5);


            var result:Bitmap = new Bitmap(vectorize.bitmapData);
            result.alpha = 0.5;
            addChild(result);


            shape = new Shape();
            shape.graphics.beginFill(0x0000FF, 1);
            shape.graphics.moveTo(path[0].x, path[0].y);
            for (var i:int = 0; i < path.length; ++i ) {
                shape.graphics.lineTo(path[i].x, path[i].y);
            }
            shape.graphics.lineTo(path[0].x, path[0].y);

            addChild(shape);

             var tf:TextField = new TextField();
            tf.text = path.length.toString();
            tf.x = 100;
            tf.y = 100;
            addChild(tf);

            stage.addEventListener(MouseEvent.CLICK, onClick);
            // entry point
        }

        private function onClick(e:MouseEvent):void {
            if (shape.hitTestPoint(e.stageX, e.stageY, true)) {
                shape.alpha = shape.alpha == 1 ? 0.5 : 1;
            }

        }

    }

}