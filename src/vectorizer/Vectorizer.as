package vectorizer   {
    import flash.display.BitmapData;
    import flash.display.Shape;
    import flash.filters.BlurFilter;
    import flash.geom.Point;

    public class Vectorizer {
        public var bitmapDataSource:BitmapData;
        public var bitmapData:BitmapData;
        private var width:int;
        private var height:int;

        private var visitedMap:Vector.<Vector.<int>>;
        private var neighboursQueue:Vector.<int>;

        private var currX:int;
        private var currY:int;
        private var color:uint;

        public function Vectorizer(bd:BitmapData) {
            bitmapDataSource = bd;
            width = bd.width;
            height = bd.height;
        }

        private function buildVisitedMap():void {
            visitedMap = new Vector.<Vector.<int>>(width, true);
            for (var i:int = 0; i < width; ++i) {
                visitedMap[i] = new Vector.<int>(height, true);
                for (var j:int = 0; j < height; ++j) {
                    visitedMap[i][j] = 0;
                }
            }
        }

        public function getShape(pixelColor:uint, compare:String = '>=', threshold:Number = 0.5):Vector.<Point> {
            color = pixelColor;

            if (bitmapData) {
                bitmapData.dispose();
                bitmapData = null;
            }

            bitmapData = new BitmapData(width, height, false, 0);
            bitmapData.threshold(bitmapDataSource, bitmapDataSource.rect, new Point(0, 0), compare, color, 0xFF00FF00);
            bitmapData.applyFilter(bitmapData, bitmapData.rect, new Point(0, 0), new BlurFilter(2,2));
            bitmapData.threshold(bitmapData, bitmapData.rect, new Point(0, 0), '!=', 0xFF000000, 0xFF00FF00);

            filterEdgePixels();

            buildVisitedMap();

            for (var i:int = 0; i < width; ++i) {
                for (var j:int = 0; j < height; ++j) {
                    if (bitmapData.getPixel(i, j)) {
                        return beginPath(i, j, threshold);
                    }
                }
            }
            return null;
        }

        private  function beginPath(x:int, y:int, threshold:Number):Vector.<Point> {
            var currentNode:Point = new Point(x, y);
            var dir:Point = null;
            var vec:Point = new Point();
            var sumAngle:Number = 0;

            neighboursQueue = new Vector.<int>();
            var nodes:Vector.<Point> = new Vector.<Point>();
            nodes.push(currentNode);

            visitedMap[x][y] = 1;
            currX = x;
            currY = y;

            while (true) {
                var prevX:int = currX;
                var prevY:int = currY;

                queueNeighbour(currX + 1, currY + 1);
                queueNeighbour(currX + 1, currY - 1);
                queueNeighbour(currX - 1, currY - 1);
                checkNeighbour(currX - 1, currY + 1);
                queueNeighbour(currX + 1, currY);
                queueNeighbour(currX - 1, currY);
                queueNeighbour(currX, currY + 1);
                queueNeighbour(currX, currY -1);

                if (checkNeighbour(currX + 1, currY + 1)
                || checkNeighbour(currX + 1, currY - 1)
                || checkNeighbour(currX - 1, currY - 1)
                || checkNeighbour(currX - 1, currY + 1)
                || checkNeighbour(currX + 1, currY)
                || checkNeighbour(currX - 1, currY)
                || checkNeighbour(currX, currY + 1)
                || checkNeighbour(currX, currY -1)) {

                    if (!dir) {
                        dir = new Point(currX - prevX, currY - prevY);
                        dir.normalize(1);
                    } else {
                        vec.x = currX - prevX;
                        vec.y = currY - prevY;
                        vec.normalize(1);
                        sumAngle +=  Math.asin(cross(dir, vec));
                        if (Math.abs(sumAngle) > threshold * Math.PI) {
                            currentNode = new Point(prevX, prevY);
                            nodes.push(currentNode);
                            dir = vec.clone();
                            sumAngle = 0;
                        }
                    }


                } else {
                    var nb:int = popNeighbour();
                    if (nb >= 0) {
                        currX = nb % width;
                        currY = nb / width;
                        dir = null;
                    } else {
                        break;
                    }
                }
            }
            return nodes;
        }

        private function popNeighbour():int {
            while (neighboursQueue.length) {
                var nb:int = neighboursQueue.pop();
                var nbX:int = nb % width;
                var nbY:int = nb / width;
                if ((bitmapData.getPixel(nbX, nbY) & 0x0000FF) != 0) {
                    return nb;
                }
            }
            return -1;
        }

        private function queueNeighbour(x:int, y:int):void {
            if (bitmapData.getPixel(x, y)) {
                if (!visitedMap[x][y] && (x == currX || y == currY || bitmapData.getPixel(currX, y) || bitmapData.getPixel(x, currY))) {
                    if ((bitmapData.getPixel(x, y) & 0x00FF00) != 0) {
                        neighboursQueue.push(x + y * width);
                        bitmapData.setPixel(x, y, 0x0000FF);
                    }
                }
            }
        }

        private function checkNeighbour(x:int, y:int):Boolean {
            if (bitmapData.getPixel(x, y)) {
                if (!visitedMap[x][y] && (x == currX || y == currY || bitmapData.getPixel(currX, y) || bitmapData.getPixel(x, currY))) {
                    visitedMap[currX][y] = visitedMap[x][currY] = visitedMap[x][y] = 1;

                    bitmapData.setPixel(x, y, 0xFF0000);
                    bitmapData.setPixel(x, currY,0xFF0000 & bitmapData.getPixel(x, currY));
                    bitmapData.setPixel(currX, y, 0xFF0000 & bitmapData.getPixel(x, currY));
                    currX = x;
                    currY = y;
                    return true;
                }
            }
            return false;
        }

        private function isPixelOnEdge(x:int, y:int):Boolean {
            return bitmapData.getPixel(x,y) && !(bitmapData.getPixel(x + 1, y + 1) && bitmapData.getPixel(x + 1, y)
            && bitmapData.getPixel(x + 1, y - 1) && bitmapData.getPixel(x, y - 1) && bitmapData.getPixel(x - 1, y - 1)
            && bitmapData.getPixel(x - 1, y) && bitmapData.getPixel(x - 1, y + 1) && bitmapData.getPixel(x, y + 1));
        }

        private function filterEdgePixels():void {
            var newBitmapData:BitmapData = new BitmapData(width, height, false, 0);
            for (var i:int = 0; i < width; ++i) {
                for (var j:int = 0; j < height; ++j) {
                    if (isPixelOnEdge(i, j)) {
                        newBitmapData.setPixel(i, j, 0x00FF00);
                    }
                }
            }
            if (bitmapData) {
                bitmapData.dispose();
                bitmapData = null;
            }
            bitmapData = newBitmapData;
        }

        private function cross(a:Point, b:Point):Number {
            return a.x * b.y - a.y * b.x;
        }

    }

}